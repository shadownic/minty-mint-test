import * as React from 'react';
import './App.scss';

import { connect } from 'react-redux';

import ToDoTable from './components/toDoTable';

class App extends React.Component {
  public render() {
    return (
      <div className="overflow">
        <ToDoTable tableId={1} />
      </div>
    );
  }
}

export default connect()(App);
