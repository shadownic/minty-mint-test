import { FormReducer } from '../../store/reducers/formReducer';
import { ToDoCardReducer } from '../../store/reducers/toDoCardReducer';
import {TagReducer} from '../../store/reducers/tagReducer'
// import { ToDoColumnReducer } from '../../store/reducers/toDoColumnReducer';

export interface FormReducer {
  [key: string]: {
    [key: string]: any;
  };
}
export interface ToDoCardReducer {
  count: number;
  [listId: number]: [Card];
}
export interface Card {
  position: number;
  cardName: string;
  listId: number;
  id: number;
}
export interface Column {
  id: number;
  title: string;
  position: number;
  tableId: number;
}
export interface ToDoColumnReducer {
  drag?: {
    column: Column;
    columnIndex: number;
  };
  count: number;
  [tableId: number]: Column[];
}

export interface Store {
  forms: FormReducer;
  column: ToDoColumnReducer;
  cards: ToDoCardReducer;
  modal: any;
  tag: TagReducer
}
