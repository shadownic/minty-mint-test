import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from './App';

import { Route, BrowserRouter as Router } from 'react-router-dom';
import Modal from './components/modals/cardDialog';

import './index.scss';
import registerServiceWorker from './registerServiceWorker';

import { Provider } from 'react-redux';

import configureStore from './store/config.store';

import { Store } from 'redux';
// import { Card } from './interfaces';

// import {State} from './store/reducers/interface'

// import {State} from './store/reducers/interface'

export interface MyStore {
  modal: {
    open: boolean;
  };
}

const store: Store<MyStore> = configureStore();

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <div>
        <Route path="/" component={App} />
        {/* <Route exact = {true} path="/toDo/:id" component= {(props: Card) => (
          <Modal value = {props}/>
        )} /> */}
        <Route
          exact={true}
          path="/toDo/:id"
          component={Modal}  
        />
      </div>
    </Router>

    {/* <App /> */}
  </Provider>,

  document.getElementById('root') as HTMLElement
);
registerServiceWorker();
