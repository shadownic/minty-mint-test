import { applyMiddleware, createStore, } from 'redux';
import thunk from 'redux-thunk';
import reducer from './reducers/root';



const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
export default function configureStore()  {
  return createStore(
    reducer,
    // enhancers: []
    composeEnhancer(applyMiddleware(thunk)),
  );
}
