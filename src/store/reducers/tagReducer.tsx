import { TAG } from '../actions/actionTypes';
import { TagReducerAction, TagAddPayload } from '../actions/tagReducer.action';

export interface TagReducer {
  count: number;
  [cardId: number]: any;
}

export default (
  state = {} as TagReducer,
  { type, payload }: TagReducerAction<TagAddPayload>
): TagReducer => {
  switch (type) {
    case TAG.ADD: {
      if (!state.count) {
        state.count = 0;
      }
      payload = payload as TagAddPayload;
      payload.id = state.count;
      state.count++;
      if (!state[payload.cardId]) {
        state[payload.cardId] = [];
      }
      return {
        ...state,
        [payload.cardId]: [...state[payload.cardId], payload]
      };
      // const apayload: any = payload;
      // apayload.id = state.count;
      // apayload.position = state.count;
      // state.count++;
      // if (!state[apayload.listId]) {
      //   state[apayload.listId] = [];
      // }
      // return {
      //   ...state,
      //   [apayload.listId]: [...state[apayload.listId], apayload]
      // }

      return { ...state, [payload.cardId]: { ...state[payload.cardId] } };
    }
    default:
      return state;
  }
};
