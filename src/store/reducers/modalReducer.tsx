import { DIALOG } from '../actions/actionTypes';

export default (state = {}, { type, payload }: any): any => {
  switch (type) {
    case DIALOG.OPEN: {
      return { ...state, [payload]: true };
    }
    case DIALOG.CLOSE: {
      const { [payload as string]: deleted, ...newState } = state;
      return { ...newState };
    }

    default:
      return state;
  }
};
