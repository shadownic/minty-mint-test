import { combineReducers } from 'redux';

// import { reducer } from 'redux-form';

import modals from './modalReducer';
// import props from './reducer';

import formReducer from './formReducer';
import toDoColumn from './toDoColumnReducer';
import tagReducer from './tagReducer';
import cardReducer from './toDoCardReducer';

// export interface Store {
//   forms: FormReducer
//   column: ToDoColumnReducer
//   cards: ToDoCardReducer
// }

export default combineReducers({
  cards: cardReducer,
  column: toDoColumn,
  forms: formReducer,
  modal: modals,
  tag: tagReducer
  // toDoList: props
});
