import { COLUMN } from '../actions/actionTypes';

import { Column, ToDoColumnReducer } from '@interfaces/store';

import {
  AddToDoColumnPayload,
  ColumnAction,
  ToggleColumnPayload,
  DeleteColumnPayload
} from '../actions/toDoColumnReducer.actions';

interface A {
  column: Column;
  before: boolean;
  dropIndex: number;
}

export default (
  state = { count: 102 } as ToDoColumnReducer,
  {
    type,
    payload
  }: ColumnAction<
    AddToDoColumnPayload | ToggleColumnPayload | DeleteColumnPayload | A | any
  >
): ToDoColumnReducer => {
  switch (type) {
    case COLUMN.DELETE: {
      payload = payload as DeleteColumnPayload;
      const { column: c, columnIndex } = payload;
      state[c.tableId].splice(columnIndex, 1);
      return { ...state, [c.tableId]: [...state[c.tableId]] };
    }
    case COLUMN.DRAG.MOVE: {
      const dropElement = payload as A;
      const dragElement = state.drag;

      const { before, dropIndex } = dropElement;

      if (dragElement) {
        const dropEl = state[payload.column.tableId][dropIndex];
        const dragEl = state[payload.column.tableId][dragElement.columnIndex];
        if (
          (before && dropIndex - 1 !== dragElement.columnIndex) ||
          (!before && dropIndex + 1 !== dragElement.columnIndex)
        ) {
          state[payload.column.tableId][dragElement.columnIndex] = dropEl;
          state[payload.column.tableId][dropIndex] = dragEl;

          return {
            ...state,
            [dropElement.column.tableId]: [
              ...state[dropElement.column.tableId]
            ],
            drag: { column: dragEl, columnIndex: dropIndex }
          };
        }
      }

      return {
        ...state,
        [dropElement.column.tableId]: [...state[dropElement.column.tableId]]
      };
    }
    case COLUMN.DRAG.START: {
      const { column, columnIndex } = payload;
      return { ...state, drag: { column, columnIndex } };
    }

    case COLUMN.ADD: {
      payload = payload as AddToDoColumnPayload;

      if (!state[payload.tableId]) {
        state[payload.tableId] = [];
      }
      payload.id = state.count;

      payload.position = state.count;
      state.count++;
      return {
        ...state,
        [payload.tableId]: [...state[payload.tableId], payload]
      };
    }
    default:
      return state;
  }
};
