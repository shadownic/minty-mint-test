import { FORM} from '../actions/actionTypes';

import {
  FormChangePayload,
  FormReducerAction
} from '../actions/formReducer.action';

export interface FormReducer {
  [key: string]: {
    [key: string]: any;
  };
}

export default (
  state = {} as FormReducer,
  { type, payload }: FormReducerAction<string | FormChangePayload>
): FormReducer => {
  switch (type) {
    case FORM.DISABLE: {
      const { [payload as string]: deleted, ...newState } = state;
      return { ...newState };
    }

    case FORM.CHANGE: {
      payload = payload as FormChangePayload;
      return {
        ...state,
        [payload.formName]: {
          ...state[payload.formName],
          [payload.inputName]: payload.value
        }
      };
    }

    default:
      return state;
  }
};
