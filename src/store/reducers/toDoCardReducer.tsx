import {
  // ADD_TO_DO_CARD,
  // DRAG_HANDLER_CARD_START,
  // DRAG_HANDLER_CARD_MOVE,
  // DELETE_COLUMN,
  // DELETE_CARD
  CARD,
  COLUMN
} from '../actions/actionTypes';

import {
  CardAction,
  DragCardStartPayload,
  ToggleCardPayload,
  DragMoveActionPayload,
  DeleteCardActionPayload
} from '../actions/toDoCardReducer.action';

import { Card } from '@interfaces';
import { DeleteColumnPayload } from '../actions/toDoColumnReducer.actions';

export interface ToDoCardReducer {
  count: number;
  [listId: number]: Card[];
  drag?: DragCardStartPayload;
}

export default (
  state = {
    count: 0
  } as ToDoCardReducer,
  {
    type,
    payload
  }: CardAction<
    | ToggleCardPayload
    | DragCardStartPayload
    | DragMoveActionPayload
    | DeleteColumnPayload
    | DeleteCardActionPayload
  >
): ToDoCardReducer => {
  switch (type) {
    case CARD.DELETE: {
      payload = payload as DeleteCardActionPayload;
      const { card, cardIndex } = payload;
      state[card.listId].splice(cardIndex, 1);

      return { ...state, [card.listId]: [...state[card.listId]] };
    }
    case COLUMN.DELETE: {
      payload = payload as DeleteColumnPayload;

      const { [payload.column.id]: deleted, ...newState } = state;
      return { ...newState };
    }
    case CARD.DRAG.MOVE: {
      if (typeof payload === 'number' && state.drag) {
        state[state.drag.card.listId].splice(state.drag.cardIndex, 1);

        state[payload] = [state.drag.card];
        const { listId } = state.drag.card;
        state.drag.card.listId = payload;

        return {
          ...state,
          [listId]: [...state[listId]],
          [payload]: [...state[payload]],
          drag: {
            card: { ...state.drag.card, listId: payload },
            cardIndex: state[payload].indexOf(state.drag.card)
          }
        };
      }
      if (state.drag) {
        payload = payload as DragMoveActionPayload;
        const {
          before,
          card: dropCard,
          cardIndex: dropIndex
        } = payload as DragMoveActionPayload;
        const { card: dragCard, cardIndex: dragIndex } = state.drag;
        const dropListId = dropCard.listId;
        const dragListId = dragCard.listId;
        const dropEl = state[dropListId][dropIndex];
        const dragEl = state[dragCard.listId][dragIndex];
        if (dragCard.listId === dropCard.listId) {
          if (
            (before && dragIndex - 1 === dropIndex) ||
            (!before && dragIndex + 1 === dropIndex)
          ) {
            state[dropListId][dropIndex] = dragEl;
            state[dragCard.listId][dragIndex] = dropEl;

            return {
              ...state,
              [dropListId]: [...state[dropListId]],
              drag: { card: dragEl, cardIndex: dropIndex }
            };
          }
        }
        if (dragCard.listId !== dropCard.listId) {
          state[dragListId].splice(dragIndex, 1);
          dragCard.listId = dropListId;
          if (before) {
            state[dropListId].splice(dropIndex, 0, dragCard);
          }
          if (!before) {
            state[dropListId].splice(dropIndex + 1, 0, dragCard);
          }
          return {
            ...state,
            [dragListId]: [...state[dragListId]],
            [dropListId]: [...state[dropListId]],
            drag: {
              card: { ...state.drag.card, listId: dropListId },
              cardIndex: state[dropListId].indexOf(dragCard)
            }
          };
        }

        return { ...state };
      }

      return { ...state };
    }
    case CARD.DRAG.START: {
      payload = payload as DragCardStartPayload;
      return { ...state, drag: payload };
    }
    case CARD.ADD: {
      const apayload: any = payload;
      apayload.id = state.count;
      apayload.position = state.count;
      state.count++;
      if (!state[apayload.listId]) {
        state[apayload.listId] = [];
      }
      return {
        ...state,
        [apayload.listId]: [...state[apayload.listId], apayload]
      };
    }

    default:
      return state;
  }
};
