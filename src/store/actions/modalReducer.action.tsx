import { DIALOG } from './actionTypes';

export function openToDoCardDialog(payload: any) {
  return {
    payload,
    type: DIALOG.OPEN
  };
}

export function closeToDoCardDialog(payload: any) {
  return {
    payload,
    type: DIALOG.CLOSE
  };
}

