import { FORM } from './actionTypes';

export interface FormReducerAction<T> {
  type: string;
  payload: T;
}

export interface FormChangePayload {
  formName: string;
  inputName: string;
  value: string;
}

export function formChangeAction(
  payload: FormChangePayload
): FormReducerAction<FormChangePayload> {
  return {
    payload,
    type: FORM.CHANGE
  };
}

export function formDisableAction(payload: string): FormReducerAction<string> {
  return {
    payload,
    type: FORM.DISABLE
  };
}
