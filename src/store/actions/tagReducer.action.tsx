import { TAG } from './actionTypes';

export interface TagReducerAction<T>{
    type: string,
    payload: T
}

export interface TagAddPayload {
    tagName: string,
    cardId: number,
    id?: number
}
// {tagName: "asd", cardId: 0}

export function addTagAction(payload: TagAddPayload): TagReducerAction<TagAddPayload> {
  return {
    payload,
    type: TAG.ADD
  };
}
