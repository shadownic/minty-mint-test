// import { TOGGLE_CARD } from './actionTypes';
import { CARD } from './/actionTypes';

import { Card } from '@interfaces';

export interface CardAction<T> {
  type: string;
  payload: T;
}

export interface DeleteCardActionPayload {
  cardIndex: number;
  card: Card;
}

export interface ToggleCardPayload {
  listId: number;
  current: number;
  drop: number;
}

export interface DragCardStartPayload {
  card: Card;
  cardIndex: number;
}
export interface DragMoveActionPayload {
  card: Card;
  cardIndex: number;
  before: boolean;
}

export function deleteCardAction(
  payload: DeleteCardActionPayload
): CardAction<DeleteCardActionPayload> {
  return {
    payload,
    type: CARD.DELETE
  };
}

export function dragCardMoveAction(
  payload: DragMoveActionPayload | number
): CardAction<DragMoveActionPayload | number> {
  return {
    payload,
    // type: DRAG_HANDLER_CARD_MOVE
    type: CARD.DRAG.MOVE
  };
}

export function dragCardStartAction(
  payload: DragCardStartPayload
): CardAction<DragCardStartPayload> {
  return {
    payload,
    // type: DRAG_HANDLER_CARD_START
    type: CARD.DRAG.START
  };
}
export function createToDoCardAction(payload: any) {
  return {
    payload,
    type: CARD.ADD
  };
}
