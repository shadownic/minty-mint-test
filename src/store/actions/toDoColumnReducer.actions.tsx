import {
  COLUMN
  // ADD_TO_DO_COLUMN,
  // DRAG_HANDLER_COLUMN_MOVE,
  // DRAG_HANDLER_COLUMN_START,
  // DELETE_COLUMN
} from './actionTypes';
import { Column } from '@interfaces';

export interface ColumnAction<T> {
  type: string;
  payload: T;
}

export interface AddToDoColumnPayload {
  tableId: number;
  title: string;
  id: number | string;
  position: number | string;
}
export interface ToggleColumnPayload {
  tableId: number;
  current: any;
  drop: any;
}
export interface DeleteColumnPayload {
  column: Column;
  columnIndex: number;
}

export function deleteColumnAction(
  payload: DeleteColumnPayload
): ColumnAction<DeleteColumnPayload> {
  return {
    payload,
    type: COLUMN.DELETE
  };
}

export function dragColumnMoveAction(payload: any) {
  return {
    payload,
    type: COLUMN.DRAG.MOVE
  };
}

export function dragColumnStartAction(payload: any) {
  return {
    payload,
    type: COLUMN.DRAG.START
  };
}
export function createToDoColumnAction(
  payload: AddToDoColumnPayload
): ColumnAction<AddToDoColumnPayload> {
  return {
    payload,
    type: COLUMN.ADD
  };
}
