export const DIALOG = {
  CLOSE: 'DIALOG_CLOSE',
  OPEN: 'DIALOG_OPEN'
};

export const TAG = {
  ADD: 'TAG_ADD'
}

export const COLUMN = {
  ADD: 'COLUMN_ADD',
  DELETE: 'COLUMN_DELETE',
  DRAG: {
    START: 'COLUMN_DRAG_START',
    MOVE: 'COLUMN_DRAG_MOVE'
  }
};
export const CARD = {
  ADD: 'CARD_ADD',
  DELETE: 'CARD_DELETE',
  DRAG: {
    START: 'CARD_DRAG_START',
    MOVE: 'CARD_DRAG_MOVE'
  }
};

export const FORM = {
  CHANGE: 'FORM_CHANGE',
  DISABLE: 'FORM_DISABLE'
};
