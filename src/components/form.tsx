import * as React from 'react';

import { Button, TextField } from '@material-ui/core';
import { connect, ConnectedComponentClass } from 'react-redux';

// import { formChangeAction } from '../store/actions/action';

import {
  formDisableAction,
  FormChangePayload,
  formChangeAction
} from '../store/actions/formReducer.action';

import { Store } from '@interfaces';
// import form from './toDoColumn';

interface ParentProps {
  onSubmitDispatchAction: any;
  formName: string;
  inputs: string[];
  buttonText: string;
  formProps?: any;
}

interface Form {
  [formName: string]: {
    [inputName: string]: string;
  };
}

interface ReduxProps {
  form: Form;
  disableForm: () => void;
  formChange: (payload: FormChangePayload) => void;
  submit: (payload: any) => void;
}

export class FormComponent extends React.Component<
  ReduxProps & ParentProps,
  {
    form: {
      formName?: string;
      value?: { [key: string]: string };
    };
  }
> {
  public inputs = this.props.inputs.reduce((result, next) => {
    result[next] = null;
    return result;
  }, {});
  public state = {
    form: {
      formName: this.props.formName,
      value: this.inputs
    }
  };

  public render() {
    // console.log(this.props.inputs);
    console.log(this.state);

    return (
      <form
        onBlur={(e: React.FocusEvent<HTMLFormElement>) => this.onBlurHandler(e)}
        onSubmit={(e: React.FormEvent<HTMLFormElement>) => this.submit(e)}
        onChange={(e: React.FormEvent<HTMLFormElement>) =>
          this.handleInputChange(e)
        }
      >
        {this.props.inputs.map((input, index) => (
          <TextField
            value={this.state.form.value[input] || ''}
            variant="outlined"
            key={index}
            name={input}
            autoFocus={true}
          />
        ))}
        <Button type="submit">{this.props.buttonText}</Button>
      </form>
    );
  }

  private onBlurHandler = (e: React.FocusEvent<HTMLFormElement>) => {
    this.props.disableForm();
  };
  private submit(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault();
    this.props.submit(this.props.form);
    this.setState({
      form: {
        value: {}
      }
    });
  }
  private handleInputChange(event: React.ChangeEvent<any>) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    if (name) {
      this.setState({ form: { value: { [name]: value } } });

      this.props.formChange({
        formName: this.props.formName,
        inputName: name,
        value
      });
    }
  }
}

const mapDispatchToProps = (
  dispatch: any,
  ownProps: ReduxProps & ParentProps
) => ({
  disableForm: () => {
    dispatch(formDisableAction(ownProps.formName));
  },
  submit: (formValue: { [key: string]: string }) => {
    console.log(formValue, ownProps.formProps);

    dispatch(
      ownProps.onSubmitDispatchAction(
        Object.assign(formValue, ownProps.formProps)
      )
    );
    dispatch(formDisableAction(ownProps.formName));
  },
  formChange: (payload: FormChangePayload) => {
    dispatch(formChangeAction(payload));
  }
});
const mapStateToProps = (state: Store, props: any) => ({
  form: state.forms[props.formName]
});
const el: ConnectedComponentClass<typeof FormComponent, ParentProps> = connect(
  mapStateToProps,
  mapDispatchToProps
)(FormComponent);
export default el;
