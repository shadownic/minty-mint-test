import * as React from 'react';

import Button from '@material-ui/core/Button';

import { RouteComponentProps, withRouter } from 'react-router-dom';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { connect } from 'react-redux';
import { Store, Card } from '@interfaces';

import { addTagAction } from '../../store/actions/tagReducer.action';

import Form from '../form';
interface Tag {
  cardId: number;
  id: number;
  tagName: string;
}

interface ReduxProps {
  tags: Tag[];
  closeDialog?: () => void;
}
interface RouteParams {
  id: string;
}
interface RouteState {
  card: Card;
}

class ToDoDialog extends React.Component<
  ReduxProps & RouteComponentProps<RouteParams, any, RouteState>
> {
  private card = this.props.location.state.card;
  public render() {
    return (
      <div>
        <Dialog
          open={true}
          onClose={this.closeDialog}
          scroll="body"
          aria-labelledby="scroll-dialog-title"
        >
          <DialogTitle id="scroll-dialog-title">
            {this.card.cardName}
            {this.props.tags.map(tag => {
              return ` #${tag.tagName}`;
            })}
          </DialogTitle>
          <DialogContent>
            <Form
              onSubmitDispatchAction={addTagAction}
              formName="tagForm"
              inputs={['tagName']}
              buttonText="add tag"
              formProps={{ cardId: this.card.id }}
            />
            <DialogContentText>
              Cras mattis consectetur purus sit amet fermentum. Cras justo odio,
              dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta
              ac consectetur ac, vestibulum at eros. Praesent commodo cursus
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.closeDialog} color="primary">
              Close
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
  private closeDialog = () => {
    this.props.history.push('/');
  };
}

const mapDispatchToProps = (
  dispatch: any,
  ownProps: RouteComponentProps
) => ({});
const mapStateToProps = (state: Store, props: RouteComponentProps) => {
  return {
    tags: state.tag[props.location.state.card.id] || []
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(ToDoDialog)
);
