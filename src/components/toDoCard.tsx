import * as React from 'react';
import { connect, ConnectedComponentClass } from 'react-redux';

import Card from '@material-ui/core/Card';
// import CardContent from '@material-ui/core/CardContent';

import { Card as ICard, Store } from '@interfaces';

import { Link } from 'react-router-dom';

import Delete from '@material-ui/icons/Delete';

import { IconButton, CardHeader } from '@material-ui/core';

import { openToDoCardDialog } from '../store/actions/modalReducer.action';
import { deleteCardAction } from '../store/actions/toDoCardReducer.action';

// import CardModal from './modals/cardDialog';

import {
  dragCardStartAction,
  dragCardMoveAction,
  DragMoveActionPayload
} from '../store/actions/toDoCardReducer.action';

interface Props {
  card: ICard;
  cardIndex: number;
}

interface ReduxProps {
  dialog: boolean;
  dragCard: {
    card: ICard;
    cardIndex: number;
  };
  deleteCard: () => void;
  openDialog: (a: string) => void;
  dragStart: () => void;
  dragMove: (payload: DragMoveActionPayload) => void;
}

export class ToDoCardComponent extends React.Component<
  ReduxProps & Props,
  {
    dialogOpen: boolean;
  }
> {
  public state = {
    name: 'toDoCard',
    dialogOpen: this.props.dialog
  };
  private dropBefore: boolean;
  public render() {
    return (
      <Card
        draggable={true}
        className="card droggable "
        onDragStart={e => this.onDragStartHandler(e)}
        onDragOver={e => this.onDragOverHandler(e)}
        onDragEnd={e => this.onDragEndHandler(e)}
        onClick={e => this.props.openDialog(this.state.name)}
      >
        <div className="header">
          {' '}
          <Link
            to={{
              pathname: `toDo/${this.props.card.id}`,
              state: { card: this.props.card }
            }}
          >
            <CardHeader
              subheader={` # ${this.props.card.id} ${this.props.card.cardName}`}
            />
          </Link>
          <IconButton onClick={this.props.deleteCard}>
            <Delete />
          </IconButton>
        </div>
      </Card>
    );
  }

  public componentWillReceiveProps(prop: any) {
    if (prop.dialog) {
      this.setState({
        dialogOpen: true
      });
    }
    if (!prop.dialog) {
      this.setState({
        dialogOpen: false
      });
    }
  }

  private onDragStartHandler = (e: React.DragEvent<HTMLDivElement>) => {
    e.currentTarget.className = `${e.currentTarget.classList} dragging`;
    this.props.dragStart();
  };
  private onDragOverHandler = (e: React.DragEvent<HTMLDivElement>) => {
    const dropBefore = () => {
      const { y, height } = e.currentTarget.getBoundingClientRect() as DOMRect;
      if (e.clientY < y + height / 2) {
        return true;
      } else {
        return false;
      }
    };
    const { card, cardIndex, dragCard } = this.props;
    if (
      dropBefore() !== this.dropBefore &&
      card.id &&
      dragCard.card.id !== card.id
    ) {
      this.dropBefore = dropBefore();
      this.props.dragMove({ card, cardIndex, before: this.dropBefore });
    }
  };
  private onDragEndHandler = (e: React.DragEvent<HTMLDivElement>) => {
    e.currentTarget.classList.remove('dragging');
  };
}

const mapDispatchToProps = (dispatch: any, ownProps: Props) => ({
  deleteCard: () => {
    dispatch(deleteCardAction(ownProps));
  },
  openDialog: (dialogName: string) => {
    dispatch(openToDoCardDialog(dialogName));
  },
  dragStart: () => {
    dispatch(dragCardStartAction(ownProps));
  },
  dragMove: (a: DragMoveActionPayload) => {
    dispatch(dragCardMoveAction(a));
  }
});
const mapStateToProps = (state: Store, props: any) => {
  const dialogName = 'toDoCard';
  return {
    dialog: state.modal[`${dialogName}`],
    dragCard: state.cards.drag || null
  };
};

const element: ConnectedComponentClass<
  typeof ToDoCardComponent,
  Props
> = connect(
  mapStateToProps,
  mapDispatchToProps
)(ToDoCardComponent);
export default element;

// export default withRouter(
//   connect(
//     mapStateToProps,
//     mapDispatchToProps
//   )(element)
// );
