import * as React from 'react';
import { connect } from 'react-redux';

import { Card, Typography } from '@material-ui/core';

import Form from './form';
import ToDoColumn from './toDoColumn';

import { Column, Store } from '@interfaces/store';

import { createToDoColumnAction } from '../store/actions/toDoColumnReducer.actions';

interface ParentProps {
  tableId: number;
}
interface ReduxProps {
  formActive: boolean;
  list: Column[];
}

export class ToDoTableComponent extends React.Component<
  ReduxProps &
    ParentProps & {
      formName: string;
    }
> {
  public state = {
    formActive: false
  };
  public render() {
    return (
      <div className="flex row">
        {this.props.list &&
          this.props.list.map((el: Column, index: number) => (
            <ToDoColumn key={el.id} column={el} columnIndex={index} />
          ))}
        {(this.state.formActive && (
          <Form
            formName={this.props.formName}
            inputs={['title']}
            buttonText="+ add column"
            onSubmitDispatchAction={createToDoColumnAction}
            formProps={{
              tableId: this.props.tableId
            }}
          />
        )) || (
          <Card className="table column add" onClick={this.activateForm}>
            <Typography>+ Add new column</Typography>
          </Card>
        )}
      </div>
    );
  }

  public componentWillReceiveProps(props: ReduxProps) {
    if (!props.formActive) {
      this.setState({
        formActive: false
      });
    }
  }
  private activateForm = () => {
    this.setState({
      formActive: true
    });
  };
}

const mapStateToProps = (state: Store, props: ParentProps) => {
  const a = 'addColumn';
  return {
    formName: a,
    list: state.column[props.tableId],
    formActive: !!state.forms[`'addColumn'`] || false
  };
};

export default connect(mapStateToProps)(ToDoTableComponent);
