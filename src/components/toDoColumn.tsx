import * as React from 'react';
import { connect, ConnectedComponentClass } from 'react-redux';

import { createToDoCardAction } from '../store/actions/toDoCardReducer.action';
import { deleteColumnAction } from '../store/actions/toDoColumnReducer.actions';

import ToDoCard from './toDoCard';

import Form from './form';

import Delete from '@material-ui/icons/Delete';

import Card from '@material-ui/core/Card';

import {
  CardActions,
  Typography,
  IconButton,
  CardHeader
} from '@material-ui/core';

import {
  dragColumnMoveAction,
  dragColumnStartAction
} from '../store/actions/toDoColumnReducer.actions';

import { dragCardMoveAction } from '../store/actions/toDoCardReducer.action';

import { Dispatch } from 'redux';

import { Column, Store } from '@interfaces';

interface Props {
  column: Column;
  columnIndex: number;
}

interface ReduxProps {
  formActive: boolean;
  formName: string;
  toDo: any[];
  dragId: number;
  deleteColumn: () => void;
  dragAddCard: () => void;
  dragStart: () => void;
  dragMove: (id: any) => void;
  addCard: (a: any) => void;
}

export class ToDoColumnComponent extends React.Component<ReduxProps & Props> {
  public state = {
    formActive: false
  };
  private dropBefore: boolean = true;

  public render() {
    return (
      <Card
        className="table column droggable "
        draggable={true}
        onDragOver={this.dragForCard}
      >
        {' '}
        <CardHeader
          draggable={true}
          onDragStart={e => this.dragStartHandler(e)}
          onDragOver={e => this.dragOverHandler(e)}
          onDragEnd={e => this.onDragEndHandler(e)}
          title={this.props.column.title}
          subheader={`${this.props.toDo.length} cards`}
          action={
            <IconButton onClick={this.props.deleteColumn}>
              <Delete />
            </IconButton>
          }
        />
        <div className="overflow vertical">
          {' '}
          {(this.props.toDo &&
            this.props.toDo.length &&
            this.props.toDo.map((item, index) => (
              <ToDoCard key={item.id} card={item} cardIndex={index} />
            ))) ||
            null}
        </div>
        <CardActions className={!this.state.formActive ? 'altForm' : ''}>
          {(this.state.formActive && (
            <Form
              onSubmitDispatchAction={createToDoCardAction}
              formName={this.props.formName}
              inputs={['cardName']}
              buttonText="+ Add new card"
              formProps={{ listId: this.props.column.id }}
            />
          )) || (
            <Typography color="textSecondary" onClick={this.activateForm}>
              + Add another card
            </Typography>
          )}
        </CardActions>
      </Card>
    );
  }
  public componentWillReceiveProps(props: Props & ReduxProps) {
    if (!props.formActive) {
      this.setState({
        formActive: false
      });
    }
  }

  private dragForCard = () => {
    if (!this.props.toDo.length) {
      this.props.dragAddCard();
    }
  };
  private dragStartHandler = (e: React.DragEvent<HTMLDivElement>) => {
    const parent = e.currentTarget.parentElement;
    if (parent) {
      parent.className = `${parent.classList} dragging`;
      e.dataTransfer.setDragImage(
        parent,
        parent.getBoundingClientRect().width / 2,
        10
      );
    }
    this.props.dragStart();
  };
  private dragOverHandler = (e: React.DragEvent<HTMLDivElement>) => {
    let dropBefore: boolean;
    const { x, width } = e.currentTarget.getBoundingClientRect() as DOMRect;
    if (e.clientX < x + width / 2) {
      dropBefore = true;
    } else {
      dropBefore = false;
    }

    if (
      dropBefore !== this.dropBefore &&
      this.props.dragId !== null &&
      this.props.column.id !== this.props.dragId
    ) {
      this.dropBefore = !this.dropBefore;
      this.props.dragMove(dropBefore);
    }
  };
  private onDragEndHandler = (e: React.DragEvent<HTMLDivElement>) => {
    const parent = e.currentTarget.parentElement;
    if (parent) {
      parent.classList.remove('dragging');
    }
  };

  private activateForm = () => {
    this.setState({
      formActive: true
    });
  };
}

const mapDispatchToProps = (dispatch: Dispatch, ownProps: Props) => ({
  deleteColumn: () => {
    dispatch(deleteColumnAction(ownProps));
  },
  dragAddCard: () => {
    dispatch(dragCardMoveAction(ownProps.column.id));
  },
  dragStart: () => {
    dispatch(dragColumnStartAction(ownProps));
  },
  dragMove: (before: boolean) => {
    dispatch(
      dragColumnMoveAction({
        column: ownProps.column,
        before,
        dropIndex: ownProps.columnIndex
      })
    );
  }
  // toggleColumns: (a: any) => {
  //   const payload = {
  //     tableId: ownProps.column.tableId,
  //     current: ownProps.column.position,
  //     drop: a
  //   };
  //   dispatch(toggleColumns(payload));
  // }
});
const mapStateToProps = (state: Store, props: Props) => {
  const a = 'addCard';
  return {
    dragId: (state.column.drag && +state.column.drag.column.id) || null,
    formName: a,
    toDo: state.cards[props.column.id] || [],
    formActive: !!state.forms[`${a}`] || false
  };
};
const form: ConnectedComponentClass<
  typeof ToDoColumnComponent,
  Props
> = connect(
  mapStateToProps,
  mapDispatchToProps
)(ToDoColumnComponent);

export default form;
